-- Usage
-- ------
--
-- ```tex
-- \usepackage[unicode]{hyperref}
-- \usepackage{fontawesome5}
-- \usepackage{currfile}
--
-- \directlua{dofile("get_date.lua")}
-- \date{\directlua{get_date("\currfilepath")}}
-- ```
--
-- ```shell
-- lualatex --shell-escape [Your LaTeX file]
-- ```
--

-- Run an external process, and capture its stdout
function check_run(command)
  local p = io.popen(command)
  local output = p:read('*a')
  p:close()
  return output:gsub("^%s*(.-)%s*$", "%1") -- trim start and end white spaces
end

function get_date(tex_file_name)
  -- Last modified date in ISO 8601-like format
  local date = check_run('git log -1 --format="%ai" ' .. tex_file_name)
  -- Short hash
  local commit = check_run('git log -1 --format="%h" ' .. tex_file_name)

  if #date > 0 then
    local href = string.format("\\href{https://gitlab.com/termoshtt/termoshtt.gitlab.io/-/commit/%s}{%s}", commit, commit)
    tex.sprint("Updated: " .. date .. " \\faIcon{git-alt} " .. href)

    -- See https://docs.gitlab.com/ee/ci/variables/predefined_variables.html
    -- for GitLab CI predefined variables
    local job_id = os.getenv("CI_JOB_ID")
    local job_url = os.getenv("CI_JOB_URL")
    if job_id and job_url then
      local href = string.format("\\href{%s}{%s}", job_url, job_id)
      tex.sprint(", Generated on \\faIcon{gitlab}: " .. href)
    end
  else
    error("Failed to get last modified date of " .. tex_file_name .. " from Git")
  end
end
