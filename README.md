# @termoshtt

| SNS     | URL |
|:--------|:----|
| GitHub  | <https://github.com/termoshtt>  |
| GitLab  | <https://gitlab.com/termoshtt>  |
| Twitter | <https://twitter.com/termoshtt> |
| Mastdon | <https://mstdn.jp/@termoshtt>   |

# 研究ノート

- [テンソルの縮約の普遍性とコエンド](https://termoshtt.gitlab.io/tensor_reduction_coend.pdf)
- [離散微分幾何1](https://termoshtt.gitlab.io/discrete_differential_geometry1.pdf)
