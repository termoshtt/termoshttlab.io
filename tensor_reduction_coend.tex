\documentclass[a4paper]{ltjsarticle}

\usepackage[japanese]{babel}
\usepackage[unicode]{hyperref}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{tikz-cd}

\newcommand\N{\mathbb{N}}

\newcommand\nat{\xrightarrow{\cdot}}
\newcommand\dinat{\xrightarrow{\cdot\cdot}}
\newcommand\op{\text{op}}
\newcommand\Vect{\text{Vect}}
\newcommand\arrowin{\;\text{in}\;}

\newcommand\Set[2]{\left\lbrace #1 \; \middle\vert \; #2 \right\rbrace}

\theoremstyle{definition}
\newtheorem{dfn}{定義}
\newtheorem{thm}{定理}

\title{テンソルの縮約の普遍性とコエンド}
\author{Toshiki Teramura (@termoshtt)}
\begin{document}
\maketitle
\begin{abstract}
マックレーン「圏論の基礎」9章``特別な極限''に現れる対角自然変換とコエンドの例として、
双対空間と元の空間のテンソル積に対して双対元を適用する操作は対角自然変換をなすことを示し、
さらにそれがコエンドの意味で普遍的になることを示す。
\end{abstract}
\footnote{
  この文章は\href{https://gitlab.com/termoshtt/termoshtt.gitlab.io}{GitLab}で管理され、
  \url{https://termoshtt.gitlab.io/tensor_reduction_coend.pdf}で公開されています。
}
\section{ベクトル空間の圏}
体$K$上のベクトル空間のなす圏を$\Vect_K$と書き、この記事では有限次元で無くても成り立つ性質を議論する。
また圏・函手・自然変換の定義や線形代数の基礎については既知とする。

\subsection{双対空間}

\begin{itemize}
  \item $V \in \Vect_K$に対して双対空間を$V^* = \Vect_K(V, K)$と書く。
  \item これには通常の線形空間の構造が入るので$V^* \in \Vect_K$となる。
  \item 射$f: V \to W \arrowin \Vect_K$がある時、$\zeta \in W^*$に対して$V^*$の元として$v \mapsto \zeta(f(v))$を割り当てる線形射を$f^*: W^* \to V^* \arrowin \Vect_K$と書く。
  \item この反変函手を$-^*: \Vect_K^\op \to \Vect_K$と書く。
\end{itemize}

\subsection{テンソル積}
ベクトル空間$V, W \in \Vect_K$がある時、集合としてのカルテシアン積$V \times W$に
ベクトル空間の構造を入れる方法はいくつか存在し、
代表的な方法として直和$V \oplus W$とテンソル積$V \otimes W$がある。直和は
\begin{equation*}
  V \oplus W = \Set{(v, w)}{v \in V, w \in W}
\end{equation*}
であって、スカラー値$a, b \in K$に対して
\begin{equation*}
  a ( v_1, w_1 ) + b (v_2, w_2) = (av_1 + bv_2, aw_1 + bw_2)
\end{equation*}
によって線形構造を入れる。

一方テンソル積は一旦大きな自由ベクトル空間を作ってから、
その中で元のベクトル空間の構造から同じとみなすべき元を同一視することで構成する。
$V \times W$を基底とする自由ベクトル空間
\begin{equation*}
  X = \bigcup_{n \in \N} \Set{\sum_{i=1}^n a_i (v_i, w_i)}{a_i \in K, (v_i, w_i) \in V \times W }
\end{equation*}
をつくる。この時$(v + v^\prime, w)$と$(v, w) + (v^\prime, w)$は$X$では別の元になる事に注意する。
これらを同一視するために
\begin{align*}
  C_r &= \Set{(v, aw+ bw^\prime) - \left(a(v, w) + b(v, w^\prime)\right) }{a,b \in K, v \in V, w, w^\prime \in W} \\
  C_l &= \Set{(av + bv^\prime, w) - \left(a(v, w) + b(v^\prime, w)\right) }{a,b \in K, v, v^\prime \in V, w \in W}
\end{align*}
によって生成される部分空間$Y = \text{Span} \; C_r \cup C_l$を考える。
すると$(v + v^\prime, w) - \left((v, w) + (v^\prime, w)\right) \in Y$なので商空間
\begin{equation*}
  V \otimes W = X / Y
\end{equation*}
を考えると両者は同じ同値類に所属する。
この時各$(v, w) \in V \times W$に対応する同値類を$v \otimes w \in V \otimes W$と書き、
この対応を$p_{V, W}: V \times W \to V \otimes W$と書くことにする。
構成よりこの$p_{V, W}$はそれぞれの線形構造について双線形関数であり、
さらに任意の双線形関数$f: V \times W \to Z$を分解する。
つまり$f$毎に(双線形でなく)線形射$f^\prime: V \otimes W \to Z \arrowin \Vect_K$が
一意に定まって$f = f^\prime p$が成り立つ。
\begin{equation*}
  \begin{tikzcd}
    V \times W \rar{f} \dar{p_{V, W}} & Z \\
    V \otimes W \urar{f^\prime} &
  \end{tikzcd}
\end{equation*}
逆にこのような分解を行える$p$から$V \otimes W$を議論することもできるが、
その場合でも存在を示すには上の構成を行うことになるので割愛する。

二つの任意の線形射$f: V \to V^\prime$と$g: W \to W^\prime$がある時、
$(v, w) \in V \times W$に対して$(f(v), g(w)) \in V^\prime \times W^\prime$を与える関数
$(f, g): V \times W \to V^\prime \times W^\prime$と
$p_{V^\prime, W^\prime}: V^\prime \times W^\prime \to V^\prime \otimes W^\prime$を合成した
関数は双線形になるので、テンソル積を通じて一意に分解される。
\begin{equation*}
  \begin{tikzcd}
    V \times W \rar{(f, g)} \dar{p_{V, W}} & V^\prime \times W^\prime \dar{p_{V^\prime, W^\prime}} \\
    V \otimes W \arrow[r, dotted, "{f \otimes g}"] & V^\prime \otimes W^\prime
  \end{tikzcd}
\end{equation*}
この線形射を$f \otimes g: V \otimes W \to V^\prime \otimes W^\prime$と書く。
これで対象と射についてそれぞれ対応が定まったので、テンソル積は双函手$\otimes: \Vect_K \times \Vect_K \to \Vect_K$になる

\section{対角自然変換}
$C^\op \times C \to B$のような形の双函手については通常の自然変換より弱い対角自然変換という概念が導入できる。
\begin{dfn}[対角自然変換]
圏$C, B$に対して二つの双函手$S, T: C^\op \times C \to B$があるとき、
任意の$f: c \to c^\prime \arrowin C$について次の図式を可換にする$B$の射$\alpha_c: S(c, c) \to T(c, c)$を
各対象$c \in C$について割り当てるものを対角自然変換と呼び、$\alpha: S \dinat T$と書く。
\begin{equation*}
  \begin{tikzcd}
    & S(c, c) \arrow[r, "{\alpha_{c}}"] & T(c,c) \arrow[dr, "{T(1, f)}"] &  \\
    S(c^\prime, c) \arrow[ur, "{S(f, 1)}"] \arrow[dr, "{S(1, f)}"] & & & T(c, c^\prime) \\
    & S(c^\prime, c^\prime) \arrow[r, "\alpha_{c^\prime}"] & T(c^\prime, c^\prime) \arrow[ur, "{T(f, 1)}"] &
  \end{tikzcd}
\end{equation*}
\end{dfn}

$C^\op \times C$を積圏として一つの圏とみることで通常の意味での自然変換$\tau: S \nat T$を定義でき、このような自然変換が存在する場合には任意の組$(c, c^\prime) \in C^\op \times C$について$B$の射$\tau_{(c, c^\prime)}: S(c, c^\prime) \to T(c, c^\prime)$が定義される事に注意する。通常の自然変換があるとき、その一部として特に二つの引数が一致しているものだけ抜き出すと、上の図式を可換にするので対角自然変換が作られる。

特に片方の双函手が定数函手のときくさび(楔)と呼ぶ。定数$d \in B$へのくさび$\alpha: S \dinat d$について、上の可換図式は右半分が縮退するので次のようになる
\begin{equation*}
  \begin{tikzcd}
    S(c^\prime, c) \arrow[d, "{S(1, f)}"] \arrow[r, "{S(f, 1)}"] & S(c,c) \arrow[d, "{\alpha_c}"] \\
    S(c^\prime, c^\prime) \arrow[r, "{\alpha_{c^\prime}}"] & d
  \end{tikzcd}
\end{equation*}

\subsection{双対元の適用による対角自然変換}
以上の導入のもとで一つ目の引数の双対空間とのテンソル積による双函手
\begin{equation}
  \hat\otimes = -^* \otimes -: \Vect_K^\op \times \Vect_K \to \Vect_K
\end{equation}
に対する対角自然変換について考える。$V \in \Vect_K$が与えられたとき、
$(\zeta, v) \in V^* \times V$について双対元の適用$\zeta(v) \in K$を与える操作を
$\phi_V: V^* \times V \to K$と書くと、これは双線形になるのでテンソル積を通じて分解できる。
\begin{equation*}
  \begin{tikzcd}
    V^* \times V \rar{\phi_V} \dar{\otimes} & K \\
    V^* \otimes V \urar{\mu_V}  &
  \end{tikzcd}
\end{equation*}
こうして得られた線形射$\mu_V: V^*\otimes V \to K \arrowin \Vect_K$を各$V \in \Vect_K$について束ねたものは
まさに上で議論した対角自然変換$\mu: \hat\otimes \dinat K$となる。

\begin{proof}
任意の$f: V \to W \arrowin \Vect_K$に対して$\mu_V: V^* \otimes V \to K$と$\mu_W: W^* \otimes W \to K$による次の図式を考えよう
\begin{equation*}
  \begin{tikzcd}
    W^* \otimes V \arrow[d, "{f^* \otimes 1}"] \arrow[r, "{1 \otimes f}"] & W^* \otimes W \arrow[d, "{\mu_W}"] \\
    V^* \otimes V \arrow[r, "{\mu_V}"] & K
  \end{tikzcd}
\end{equation*}
テンソル積$W^* \otimes V$の元は有限個の$W^*$の元$\{ \xi_1, \ldots, \xi_n \}$と$V$の元$\{ v_1, \ldots, v_m \}$を使って
有限和$\sum_{i, j} a_{ij} \xi_i \otimes v_j$の形でかけるので、
線形射だけからなるこの図式の可換性を示すには一つの項$\xi \otimes v$について示せば十分である。
すると双対射の性質から次が成り立つ。
\begin{equation*}
  \left(\mu_V \circ f^* \otimes 1\right)(\xi \otimes v)
  = \mu_V(f^*(\xi) \otimes v)
  = f^*(\xi)(v)
  = \xi(f(v))
  = \mu_W(\xi \otimes f(v))
  = \left(\mu_W \circ 1 \otimes f\right)(\xi \otimes v)
\end{equation*}
よって上の図式は可換であり、
$\mu: \hat\otimes \dinat K$は片方が定数函手$K \in \Vect_K$である対角自然変換、すなわちくさびである。
\end{proof}

\subsection{普遍くさびとコエンド}
極限において普遍錐を考えたように、定数函手への普遍なくさび$\alpha: S \dinat d$を考える
\begin{dfn}[コエンド]
  圏$C, B$と双函手$S: C^\op \times C \to B$が与えられたとき、
  定数函手$d \in B$へのくさび$\alpha: S \dinat d$が普遍的であるとは、
  任意のくさび$\beta: S \dinat b$に対して$h: d \to b$が存在し、
  $\beta = h\alpha$と分解できる事を指す。
  この普遍的なくさび$\alpha$と定数$d$をコエンドと呼ぶ。
\end{dfn}

\begin{thm}
くさび$\mu: \hat\otimes \dinat K$はコエンドである
\end{thm}
\begin{proof}
  任意のくさび$\tau: \hat\otimes \dinat Z$に対して$h: K \to Z$があって$\tau = h \mu$と分解出きることを示す。
  つまり任意の$f: V \to W \in \Vect_K$に対して次の図式が可換になる事を示す。
  \begin{equation*}
    \begin{tikzcd}
      & W^* \otimes W \arrow[r, "{\mu_{W}}"] \arrow[ddr, "{\tau_{W}}" near end] & K \arrow[dd, dotted, "h"] \\
        W^* \otimes V \urar{1 \otimes f} \drar{f^* \otimes 1} & & \\
      & V^* \otimes V \arrow[r, "{\tau_V}"] \arrow[uur, "{\mu_V}" near end] & Z
    \end{tikzcd}
  \end{equation*}

  まず$\tau$は$\Vect_K$の任意の対象に対して定義されないといけないので、
  特に$K \in \Vect_K$についても$\tau_K: K^\op \otimes K \to Z$が定義される。
  $K$における恒等射$1^*: K \to K$は線形なので$K^*$の元になり、
  さらにこれ一つで$K^*$の基底になることに注意する。
  すると$\tau_K$は線形射でないといけないので、$M = \tau_K(1^* \otimes 1) \in Z$によって
  任意の$\lambda \in K^*$と$x \in K$について
  \begin{equation}
    \tau_K(\lambda \otimes x) = M \lambda(x)
  \end{equation}
  と定まり$\tau_K = M \mu_K$が成り立つ。
  これより$h(x) = Mx$でないといけないことが分かる。

  次に任意の$V \in \Vect_K$を一つ選んで$f: V \to K \in \Vect_K$を考える。
  $\tau$はくさびなので次の図式が可換になる。
  \begin{equation*}
    \begin{tikzcd}
      K^* \otimes V \arrow[d, "{f^* \otimes 1}"] \arrow[r, "{1 \otimes f}"] & K^* \otimes K \arrow[d, "{\tau_K}"] \\
      V^* \otimes V \arrow[r, "{\tau_V}"] & K
    \end{tikzcd}
  \end{equation*}
  $1^* \otimes v \in K^* \otimes V$についてこの図式を追跡すると、
  \begin{equation}
    \left(\tau_K \circ 1 \otimes f\right)(1^* \otimes v) = \tau_K(1^* \otimes f(v)) = Mf(v)
  \end{equation}
  及び、$f^*: K^* \to V^*$は$v \mapsto 1^* f(v) = f(v)$すなわち$f^*(1^*) = f$に注意すると、
  \begin{equation}
    \left(\tau_V \circ f^* \otimes 1\right)(1^* \otimes v) = \tau_V(f \otimes v)
  \end{equation}
  可換性から両者が一致するので、$\tau_V(f \otimes v) = Mf(v)$となり、つまり$\tau_V = M \mu_V$である。
  以上より対角自然変換として$\tau = M \mu$が成り立つ。
\end{proof}
\end{document}
